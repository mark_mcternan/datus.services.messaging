﻿namespace Datus.Services.Messaging
{
    using System.Net.Mail;

    public class SmtpClientProxy : ISmtpClient
    {
        // instance vars
        private readonly string _host;
        private readonly int _port;

        // constructor
        public SmtpClientProxy(string host, int port)
        {
            _host = host;
            _port = port;
        }

        #region attributes
        /// <summary>
        /// Gets the hostname of a SMTP Server
        /// </summary>
        public string Host
        {
            get { return _host; }
        }

        /// <summary>
        /// Gets the port of a SMTP Server
        /// </summary>
        public int Port
        {
            get { return _port; }
        }
        #endregion attributes

        #region methods
        public bool Send(MailMessage message)
        {
            // instantiate and dispose of the .Net SmtpClient
            using (var smtp = new SmtpClient(_host, _port))
            {
                smtp.Send(message);
                return true;
            }
        }
        #endregion methods
    }
}
