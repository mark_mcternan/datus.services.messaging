# README #

### What is this repository for? ###

* A collection of interfaces and classes for a basic app.config configurable TraceListener system which anticipates a simple DI framework.

* Version 1.0

### How do I get set up? ###

* Add files as needed.  Add appropriate Trace Listening configurations to app.config  Implement and expand as needed.

### Contribution guidelines ###

* None

### Who do I talk to? ###

* mark@datus.io