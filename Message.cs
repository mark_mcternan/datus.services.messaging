﻿namespace Datus.Services.Messaging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class Message
    {
        // instance vars
        private readonly string _msg;
        private readonly MessageType _type;

        // constructor
        public Message(string message, MessageType type)
        {
            _msg = message;
            _type = type;
        }
        
        public string Text
        {
            get { return _msg; }
        }

        public MessageType Type
        {
            get { return _type; }
        }
    }
}
