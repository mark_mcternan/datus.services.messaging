﻿namespace Datus.Services.Messaging
{
    using System.Net.Mail;

    /// <summary>
    /// An interface intended for wrapping the concrete System.Net.Mail SmtpClient so that the SMTP subsystem is both injectable & testable.
    /// </summary>
    public interface ISmtpClient
    {
        ///<summary>
        /// Sends a System.Net.Mail.MailMessage
        ///</summary>
        /// <param name="message">A MailMessage to send</param>
        /// <returns>true upon successful send</returns>
        bool Send(MailMessage message);
    }
}
