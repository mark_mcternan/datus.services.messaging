﻿namespace Datus.Services.Messaging
{
    using System;

    // used for indicating seriousness of message
    [Flags]
    public enum MessageType
    {
        Debug = 1,
        Info = 2,
        Warning = 4,
        Failure = 8,
        AdminAlert = 16
    }
}
