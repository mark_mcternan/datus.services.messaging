﻿namespace Datus.Services.Messaging
{
    using System;
    using System.Configuration;
    using System.Diagnostics;

    public class AdminEventLogger
    {
        // class vars
        private static readonly string host;
        private static readonly int port;
        private static readonly string fromAddress;
        private static readonly string toAddresses;
        private static readonly ILogger logger;
        
        /// <summary>
        /// Static constructor
        /// </summary>
        static AdminEventLogger()
        {
            AdminEventLogger.host = ConfigurationManager.AppSettings["AdminAlert.SMTP.Host"];
            AdminEventLogger.port = Convert.ToInt32(ConfigurationManager.AppSettings["AdminAlert.SMTP.Port"]);
            AdminEventLogger.fromAddress = ConfigurationManager.AppSettings["AdminAlert.SMTP.From"];
            AdminEventLogger.toAddresses = ConfigurationManager.AppSettings["AdminAlert.SMTP.Recipients"];
            AdminEventLogger.logger = ServiceResolver.GetService<ILogger>();
        }

        #region methods
        public void SendAlert(Message message)
        {
            try
            {
                var smtp = new SmtpClientProxy(AdminEventLogger.host, AdminEventLogger.port);
                var msg = new System.Net.Mail.MailMessage(AdminEventLogger.fromAddress,
                                                          AdminEventLogger.toAddresses,
                                                          Enum.GetName(typeof(MessageType), message.Type),
                                                          message.Text);
                if (!smtp.Send(msg))
                {
                    throw new InvalidOperationException("Failed to send Admin Alert");
                }
            }
            catch(Exception e)
            {
                // bury exception, but log it
                AdminEventLogger.logger.Write(new Message(String.Format("{0}: ERROR - FAILED TO SEND ADMIN ALERT. {1}", this.GetType().Name, e.Message), MessageType.Failure));
            }
        }
        #endregion methods
    }
}
