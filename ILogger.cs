﻿namespace Datus.Services.Messaging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface ILogger
    {
        bool IsDebugging { get; }

        void Write(Message message);

        void Write(string message, MessageType type);
    }
}
