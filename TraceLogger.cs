﻿namespace Datus.Services.Messaging
{
    using System;
    using System.Configuration;
    using System.Diagnostics;

    /// <summary>
    /// A Trace Listener implmentation which routes trace messages based on MessageType
    /// </summary>
    public class TraceLogger : ILogger
    {
        // instance vars
        private readonly bool _debug = Convert.ToBoolean(ConfigurationManager.AppSettings["Debug.Enabled"]);
        private readonly bool _adminAlerts = Convert.ToBoolean(ConfigurationManager.AppSettings["AdminAlerts.Enabled"]);

        #region attributes
        // Is Application Debugging Enabled?
        public bool IsDebugging
        {
            get { return _debug; }
        }
        #endregion attributes

        #region methods
        public void Write(Message message)
        {
            // bail if debug and not debugging
            if (message.Type == MessageType.Debug && !this.IsDebugging)
            {
                return;
            }

            // print line
            Trace.WriteLine(message.Text, Enum.GetName(typeof(MessageType), message.Type));

            // special handling for Admin Alerts
            if (message.Type == MessageType.AdminAlert && _adminAlerts)
            {
                var admin = new AdminEventLogger();
                admin.SendAlert(message);
            }
        }

        public void Write(string message, MessageType type)
        {
            this.Write(new Message(message, type));
        }
        #endregion methods
    }
}
